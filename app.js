var app = angular.module('flapperNews', []);

app.controller('MainCtrl', [
'$scope',
function($scope){
  $scope.header = 'Flapper News';
  $scope.postTitle = 'Add A New Post'
  $scope.posts = [
  	{title: 'Post 1', upvotes: 3},
  	{title: 'Post 2', upvotes: 4},
  	{title: 'Post 3', upvotes: 7}
  ];
  $scope.addPost = function() {
  	if(!$scope.title || $scope.title == '') {return;};
  	$scope.posts.push({
  		title: $scope.title,
  		link: $scope.link,
  		upvotes: 0
  	});
  	$scope.title = '';
  	$scope.link = '';
  };
  $scope.incrementUpvotes = function(post) {
  	post.upvotes += 1;
  };


}]);